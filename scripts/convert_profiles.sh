#!/bin/bash

# Copyright (c) 2020 Kevin R Croft <krcroft@gmail.com>
# SPDX-License-Identifier: GPL-2.0-or-later

# A helper-script to covert and aggregate multiple sample-based
# perf profiles into a single Automatic Feedback Directory
# Optimization (AutoFDO) record for GCC and LLVM, respectively.
#
# Usage: merge_profiles.sh /path/to/profiled-BINARY

# Pre-requisite packages:
#   - clang-<version>, ie: clang-10
#   - llvm-<version>-dev, ie: llvm-10
#   - AutoFDO https://github.com/google/autofdo
#     configured --with-llvm=$(which llvm-config-<ver>),
#     ie: ./configure --with-llvm=/usr/bin/llvm-config-10
#     sudo make install, such that the following
#     are in PATH:
#       - create_gcov
#       - create_llvm_prof
#       - profile_merger

set -euo pipefail

# Get the binary name
prog="${1:-}"
if [[ ! -x "${prog}" ]]; then
	echo "Please provide the path to the profiled binary"
	exit 1
fi

# Move to our repo root
cd "$(git rev-parse --show-toplevel)"

for prof in $(find -name '*.prof'); do
	name="${prof%.*}"

	# Convert to GCC's AutoFDO format
	create_gcov --binary="${prog}" --profile="$name.prof" -gcov="$name.afdo" -gcov_version=1

	# Convert to LLVM's RAW profile format
	create_llvm_prof --binary="${prog}" --profile="$name.prof" --out="$name.profraw"

	echo "Converted $prof to GCC's afdo and LLVM's profraw"
done

echo "merging GCC AutoFDO profiles"
profile_merger -gcov_version=1 -output_file=current.afdo tests/*/*.afdo

echo "merging LLVM AutoFDO profiles"
llvm-profdata-10 merge -sample -output=current.profraw tests/*/*.profraw
