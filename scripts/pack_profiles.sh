#!/bin/bash

# Copyright (c) 2020 Kevin R Croft <krcroft@gmail.com>
# SPDX-License-Identifier: GPL-2.0-or-later

# A helper-script to zip the profiles and dosbox binary.

set -euo pipefail

# Get the binary name
prog="${1:-}"
if [[ ! -x "${prog}" ]]; then
	echo "Please provide the path to the profiled binary"
	exit 1
fi

# Get the absolute path to the binary
prog="$(realpath "$prog")"
progbase="$(basename "$prog")"

# Move to our repo root
cd "$(git rev-parse --show-toplevel)"

# Temporary copy the binary so its package with the profiles
cp -l -f "$prog" tests/

tar -cv "tests/$progbase" tests/*/*.prof | zstd -v -T0 -16 --long > archives/profiles.tar.zst

# Purge the binary
rm -f "tests/$progbase"
