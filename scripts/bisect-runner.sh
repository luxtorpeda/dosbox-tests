#!/bin/bash
set -euo pipefail
script_path="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
log="git-bisect.log"

git clean -fdx     &> /dev/null
./autogen.sh       &>  "$log"
./configure        &>> "$log"
make -j "$(nproc)" &>> "$log"
dosbox="$PWD/src/dosbox"

cd "$script_path"
./scripts/run.sh -d "$dosbox" "$@"
