#!/bin/bash
set -euo pipefail

# Copyright (c) 2019-2020 Kevin R Croft <krcroft@gmail.com>
# SPDX-License-Identifier: GPL-2.0-or-later

# This simple script unpacks archive/*.tar.zst files into
# directories of the same name, in workspaces/.
#
# For example:
#   archives/hello.tar.zst will be extracted to:
#   workspaces/hello/

set -euo pipefail
shopt -s nullglob
IFS=$'\n'

function cleanup()
{
    rm -rf "$TEMPDIR"
}

function init()
{
    # Move to the root of our repo
    cd "$(git rev-parse --show-toplevel)" > /dev/null
    TEMPDIR="workspaces/temp"
    mkdir -p "$TEMPDIR"
    trap cleanup EXIT
}

function extract()
{
    archives=( archives/*.tar.zst )
    for archive in "${archives[@]}"; do
        filename="$(basename -- "$archive" .tar.zst)"

		# Skip our profile results
		if [[ "$filename" == "profiles" ]]; then
			continue
		fi

        target="workspaces/${filename%.*}"
        if [[ -d "$target" && "$target" -nt "$archive" ]]; then
            echo "Skipping $archive, $target already exists"
            continue
        fi

        # If extraction fails, trap executes and we cleanup
        zstd -qdc "$archive" | tar -x -C "$TEMPDIR" --strip 1
        mv "$TEMPDIR" "$target"
        mkdir -p "$TEMPDIR"
        echo "Extracted $archive to $target"
    done
}

# run it
init
extract


