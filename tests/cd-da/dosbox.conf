[sdl]
fullscreen       = false
vsync            = false
fullresolution   = desktop
windowresolution = original
output           = opengl
texture_renderer = auto
capture_mouse    = onstart middlerelease
sensitivity      = 100
waitonerror      = true
priority         = higher,normal
mapperfile       = mapper-sdl2-git.map

[dosbox]
machine  = vgaonly
captures = capture
memsize  = 16
startup_verbosity=quiet

[render]
frameskip = 0
aspect    = true
scaler    = none

[cpu]
core      = auto
cputype   = auto
cycles    = fixed 12000
cycleup   = 10
cycledown = 20

[mixer]
nosound   = false
rate      = 48000
blocksize = 2048
prebuffer = 25

[midi]
mpu401     = none
mididevice = none
midiconfig = 

[sblaster]
#  sbtype: Type of Sound Blaster to emulate. 'gb' is Game Blaster.
#          Possible values: sb1, sb2, sbpro1, sbpro2, sb16, gb, none.
#  sbbase: The IO address of the Sound Blaster.
#          Possible values: 220, 240, 260, 280, 2a0, 2c0, 2e0, 300.
#     irq: The IRQ number of the Sound Blaster.
#          Possible values: 7, 5, 3, 9, 10, 11, 12.
#     dma: The DMA number of the Sound Blaster.
#          Possible values: 1, 5, 0, 3, 6, 7.
#    hdma: The High DMA number of the Sound Blaster.
#          Possible values: 1, 5, 0, 3, 6, 7.
# sbmixer: Allow the Sound Blaster mixer to modify the DOSBox mixer.
# oplmode: Type of OPL emulation. On 'auto' the mode is determined by 'sbtype'.
#          All OPL modes are AdLib-compatible, except for 'cms'.
#          Possible values: auto, cms, opl2, dualopl2, opl3, opl3gold, none.
#  oplemu: Provider for the OPL emulation. 'compat' provides better quality,
#          'nuked' is the default and most accurate (but the most CPU-intensive).
#          See sblaster.oplrate as well.
#          Possible values: default, compat, fast, mame, nuked.
# oplrate: Sample rate of OPL music emulation. Use 49716 for the highest
#          quality (set the mixer.rate accordingly).
#          Possible values: 44100, 49716, 48000, 32000, 22050, 16000, 11025, 8000.
sbtype  = sb16
sbbase  = 220
irq     = 7
dma     = 1
hdma    = 5
sbmixer = true
oplmode = auto
oplemu  = default
oplrate = 44100

[gus]
#      gus: Enable the Gravis Ultrasound emulation.
#  gusrate: Sample rate of Ultrasound emulation.
#           Possible values: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
#  gusbase: The IO base address of the Gravis Ultrasound.
#           Possible values: 240, 220, 260, 280, 2a0, 2c0, 2e0, 300.
#   gusirq: The IRQ number of the Gravis Ultrasound.
#           Possible values: 5, 3, 7, 9, 10, 11, 12.
#   gusdma: The DMA channel of the Gravis Ultrasound.
#           Possible values: 3, 0, 1, 5, 6, 7.
# ultradir: Path to Ultrasound directory. In this directory
#           there should be a MIDI directory that contains
#           the patch files for GUS playback. Patch sets used
#           with Timidity should work fine.

gus      = false
gusrate  = 44100
gusbase  = 240
gusirq   = 5
gusdma   = 3
ultradir = C:\ULTRASND

[speaker]
# pcspeaker: Enable PC-Speaker emulation.
#    pcrate: Sample rate of the PC-Speaker sound generation.
#            Possible values: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
#     tandy: Enable Tandy Sound System emulation. For 'auto', emulation is present only if machine is set to 'tandy'.
#            Possible values: auto, on, off.
# tandyrate: Sample rate of the Tandy 3-Voice generation.
#            Possible values: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
#    disney: Enable Disney Sound Source emulation. (Covox Voice Master and Speech Thing compatible).

pcspeaker = false
pcrate    = 44100
tandy     = off
tandyrate = 44100
disney    = false

[dos]
xms            = true
ems            = false
umb            = true
