# dosbox-tests

Mechanized regression testing for DOSbox, with bisect-support for Git
repositories and headless modes for CI chains.

Currently supported and tested on Linux; feedback welcome on macOS
and MSYS[1/2] on Windows.

## Try it

```
git clone https://gitlab.com/luxtorpeda/dosbox-tests.git`
cd dosbox-tests
./scripts/runs.sh -d /path/to/your/dosbox
```

Sample output:

```
./scripts/run.sh -d /usr/src/dosbox-staging/src/dosbox
Configuration:
  - dosbox: /usr/src/dosbox-staging/src/dosbox
  - testcases: memsize sdl-modes
  - parallelism: 24

memsize: running 8 tests
  - running ldfix-32 [passed]
  - running loadfix [passed]
  - running ldfix-96 [passed]
  - running mem-1 [passed]
  - running mem-3 [passed]
  - running mem-8 [passed]
  - running mem-16 [passed]
  - running mem-63 [passed]

sdl-modes: running 16 tests
  - running f-no-o-o [passed]
  - running f-no-s-s [passed]
  - running f-no-t-o [passed]
  - running f-no-t-s [passed]
  - running f-vs-o-o [passed]
  - running f-vs-s-s [passed]
  - running f-vs-t-o [passed]
  - running f-vs-t-s [passed]
  - running w-no-o-o [passed]
  - running w-no-s-s [passed]
  - running w-no-t-o [passed]
  - running w-no-t-s [passed]
  - running w-vs-o-o [passed]
  - running w-vs-s-s [passed]
  - running w-vs-t-o [passed]
  - running w-vs-t-s [passed]
```

## Add a Test-Case

[TODO] Add a script to create a new templatized test where the developer
swaps in actual content. This will simplify these notes and allow the
template to evolve under the hood.

## Bisect A Bug

[TODO] Step through an example auto-bisection that isolates the introduction
of a bug to a specific historic commit.

